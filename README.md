# dadoware.py

Um simples script para gerar senhas fortes e facéis de serem lembradas utilizando o método [Diceware](https://en.wikipedia.org/wiki/Diceware) e um dicionário em português. 

## Instalação 

Faça o download do repositório no formato preferido (zip, tar.gz, tar.bz2 ou tar) e então descompacte o arquivo.

Em um terminal, navegue até a pasta do repositório e utilize o comando **chmod** para liberar o script para ser executado pelo seu usuário:

`chmod +x dadoware.py`

Agora é só rodar o script! Se preferir, use como argumento o número de palavras que você deseja que sua passphrase possua.
Por exemplo, para uma senha com dez palavras, use o seguinte comando:

`./dadoware.py 10`

