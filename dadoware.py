#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import randint
from time import sleep
import sys

'''
    Gerador de passphrases aleatórias usando o método DiceWare.
'''

if len(sys.argv) > 1:
    nPalavras = sys.argv[1]
else:
    nPalavras = input('Digite com quantas palavras você deseja sua passphrase '
                      'e pressione enter\n')

# Testa as condições de nPalavras
while nPalavras == '' or not nPalavras.isdigit():
    nPalavras = input('Tente outra vez. Digite o número de palavras que sua '
                      'senha deve conter')
while int(nPalavras) < 6:
    nPalavras = input('Poucas palavras para uma senha segura. O número mínimo '
                      'de palavras é 6.\nDigite com quantas palavras você deseja '
                      'criar sua passphrase e pressione enter\n')

passphrase = []
with open("dadoware.txt", "r") as wl:
    lines = wl.readlines()
    for x in range(int(nPalavras)):
        serie = []
        for n in range(5):
            roll = randint(1,6)
            serie.append(roll)
        index = ''
        for i in serie:
            index += ''.join(str(i))
        for x, line in enumerate(lines):
            if index in line:
                passphrase.append(line[6:-1])

    print(*passphrase, sep=" ")
